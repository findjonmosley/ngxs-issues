import { Component, OnInit } from '@angular/core';
import { DataService } from "./shared/services/data.service";

import { NgxsModule } from '@ngxs/store';
import { Store, Select } from "@ngxs/store";
import { AppState } from "./shared/app.state";
import {
  LoadData,
  SetString
} from "./shared/app.actions";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  appData = {};

  constructor(private data:DataService, private store:Store) {}

  ngOnInit() {

    this.store.select(state => state.appState)
      .subscribe(state => {
        this.appData = state;
    });

    this.store.dispatch(new LoadData())
      .subscribe(state => {
        this.store.dispatch(new SetString('test'));
      });

  }
}
