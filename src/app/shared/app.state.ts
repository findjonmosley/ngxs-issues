import { State, Action, StateContext, Selector, Store } from "@ngxs/store";
import { DataService } from "../shared/services/data.service";
import { 
    LoadData, SetString
} from "./app.actions";
import { tap, catchError } from 'rxjs/operators';

export interface appStateModel {
    wiljam: string;
    data: object[];
}

@State<appStateModel> ({
    name: 'appState',
    defaults: {
        wiljam: '123',
        data: []
    }
})
export class AppState {

    constructor(private data: DataService, private store: Store) {}

    @Action(LoadData)
    loadData(ctx: StateContext<appStateModel>, action: LoadData) {
        const state = ctx.getState();
        return this.data.loadData().pipe(
            tap( response => {
                ctx.setState({
                    ...state,
                    data: response
                });
            })
        );   
    }

    @Action(SetString)
    setString(ctx: StateContext<appStateModel>, action: SetString) {
        const state = ctx.getState();
        ctx.setState({
            ...state,
            wiljam: action.payload
        });
    }
}