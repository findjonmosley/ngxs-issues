import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }

  loadData(): Observable<any>  {
    return this.http.get('http://localhost:3000/products');
  }
  postData(): Observable<any>  {
    return this.http.post('http://localhost:3000/products', {name: 'william'});
  }
}
