import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DataService } from './data.service';
let httpMock: HttpTestingController;

describe('DataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DataService
      ],
      imports: [
        HttpClientTestingModule
      ],
    });
  });

  beforeEach(() => {
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', inject([DataService], (service: DataService) => {
    expect(service).toBeTruthy();
  }));
});
