export class LoadData {
    static readonly type = '[Load data]';
}
export class SetString {
    static readonly type = '[Set String]';
    constructor(public payload: string) {}
}