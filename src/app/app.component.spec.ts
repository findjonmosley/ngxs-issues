import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AppComponent } from './app.component';

import { NgxsModule } from '@ngxs/store';
import { Store, Select } from "@ngxs/store";
import { AppState } from "./shared/app.state";
import {
  LoadData
} from "./shared/app.actions";

describe('AppComponent', () => {
  let httpMock: HttpTestingController;
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let store: Store;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        HttpClientTestingModule,
        NgxsModule.forRoot([AppState])
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture   = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    httpMock  = TestBed.get(HttpTestingController);
    store     = TestBed.get(Store);
    fixture.detectChanges();
  });

  it('it verifies a GET request was made', () => {
    const dummyUsers = [
      { login: 'John' },
      { login: 'Doe' }
    ];

    // THIS CALL DOES NOT NEED TO BE MADE BECAUSE YOU HAVE RUN THE COMPONENT ALREADY, WHICH WILL HAVE RUN ALL THE METHODS INCLUDING NGONINIT
    // store.dispatch(new LoadData());

    const resp = httpMock.match({url: 'http://localhost:3000/products', method: 'GET'});

    resp[0].flush(dummyUsers);

    store.selectOnce(state => state.appState).subscribe(state => {
      expect(state.data).toEqual(dummyUsers);
    });
    
  });

});
